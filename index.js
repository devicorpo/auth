const express = require("express");
const app = express();
const dotenv = require("dotenv");
const mongoose = require("mongoose");
dotenv.config();

//import Routes
const authRouter = require("./routes/auth");
const postRouter = require("./routes/posts");
//connect to db
mongoose.connect(process.env.DB_CONNECT, () => console.log("connected to db"));

//Middlewares
app.use(express.json());

//Route middlewares
app.use("/api/user", authRouter);
app.use("/api/posts", postRouter);
let port = process.env.PORT;
if (port === null || port === "") {
  port = 3000;
}
app.listen(port, () => console.log("Server up and running"));
