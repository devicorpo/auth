const router = require("express").Router();
const User = require("../model/User");
const { registrationVal } = require("../validation");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
router.post("/registor", async (req, res) => {
  const { error } = registrationVal(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  const emailExits = await User.findOne({ email: req.body.email });
  if (emailExits) return res.status(409).send("User already exists");
  const hashPassword = await bcrypt.hash(req.body.password, 10);
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashPassword,
  });
  try {
    const savedUser = await user.save();
    res.send(savedUser);
  } catch (error) {
    res.status(400).send(error);
  }
});
router.post("/login", async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(404).send("Email doesn't found");

  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(404).send("Password doesn't match");
  const authToken = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);

  res.send({
    authToken: authToken,
    user: user,
  });
});
module.exports = router;
