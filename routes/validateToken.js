const jwt = require("jsonwebtoken");
module.exports = function verify(req, res, next) {
  const token = req.header("authToken");
  if (!token) return res.status(501).send("Access Denied");
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    console.log(verified);
    req.user = verified;
    next();
  } catch (err) {
    res.status(501).send(err);
  }
};
